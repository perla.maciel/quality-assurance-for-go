package src

import 	( 
    "testing"
    "github.com/stretchr/testify/assert"
)


func Testfib(t *testing.T) {
  assert := assert.New(t)

  assert.Equal(fib(11), 89, "The two numbers should be the same.")
  assert.Equal(fib(9), 34, "The two numbers should be the same.")
}

func TestAverage(t *testing.T) {
  assert := assert.New(t)
  xs := []float64{98,93,77,82,83}
  assert.Equal(Average(xs), 86.6, "The two numbers should be the same.")
  xs1 := []float64{98,93,99,82,83}
  assert.Equal(Average(xs1), 91.0, "The two numbers should be the same.")
}


func TestAdd(t *testing.T) {
  assert := assert.New(t)

  assert.Equal(Add(1,2,3), 6, "The two numbers should be the same.")
  assert.Equal(Add(3,3,3), 9, "The two numbers should be the same.")
}

func TestEven(t *testing.T) {
  assert := assert.New(t)
  nextEven := makeEvenGenerator()
  assert.Equal(nextEven(), 0, "The two numbers should be the same.")
  assert.Equal(nextEven(), 2, "The two numbers should be the same.")
}

func TestGreater(t *testing.T){
    assert := assert.New(t)
    
    assert.Equal(Greater(1,2,6,3,9,8,4), 9, "The two numbers should be the same.")
}

func TestTimeWIthWords(t *testing.T) {
  assert := assert.New(t)

  assert.Equal(TimeWithWords(5,47), "thirteen minutes to six", "The strings should be the same.")
  assert.Equal(TimeWithWords(3,9), "nine minutes past three", "The strings should be the same.")
}

func TestCompare(t *testing.T) {
  assert := assert.New(t)
  
  assert.True(Compare(hour(9),"nine"))
  assert.False(Compare(hour(1),"eleven"))
  assert.True(Compare(firsthalf(9),"nine minutes past "))
  assert.False(Compare(firsthalf(23),"thirteen minutes past "))
  assert.True(Compare(otherhalf(9),"nine minutes to "))
  assert.False(Compare(otherhalf(15),"seven minutes to "))
}
