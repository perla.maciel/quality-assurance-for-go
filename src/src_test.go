package src

import 	( 
    "fmt"
)

func Example(){
	//Ejemplos de funciones 
	/*1. Se saca promedio en una función 
	y retorna el valor rquerido	*/
	xs := []float64{98,93,77,82,83}
	fmt.Println(Average(xs))

	/*2.Hace la suma de los numero enteros 
	dados como parametro*/
	fmt.Println(Add(1,2,3))

	/*3.Función que saca la mitad de un numero*/
	nextEven := makeEvenGenerator()
	fmt.Println(nextEven()) // 0
	fmt.Println(nextEven()) // 2
	fmt.Println(nextEven()) // 4
	/*4.Función que imprime la hora en letras*/
	fmt.Println(TimeWithWords(5,47))
	/*5.Función fibonacci*/
	fmt.Println(fib(11))
	//Output:
	//86.6
	//6
	//0
	//2
	//4
	//thirteen minutes to six
	//89
}
