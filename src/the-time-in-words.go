package src

//Esta funcion muestra la hora en letra
func TimeWithWords(h int32, m int32) string{
    hora := ""
    if m == 0 {
        hora = hour(h) + "o' clock"
    }else if m >= 1 && m <= 60 {
        if m >= 1 && m <= 30 {
            hora = firsthalf(m) + hour(h)
        }else {
            hora = otherhalf(60-m) + hour(h+1)
        }
    }    
    return hora
}

func hour(h int32) string{
    aux := ""
    switch h {
        case 1:
            aux = "one"
        case 2:
            aux = "two"
        case 3:
            aux = "three"
        case 4:
            aux = "four"
        case 5:
            aux = "five"
        case 6:
            aux = "six"
        case 7:
            aux = "seven"
        case 8:
            aux = "eight"
        case 9:
            aux = "nine"
        case 10:
            aux = "ten"
        case 11:
            aux = "eleven"
        case 12:
            aux = "twelve"    
    }  
    return aux
}

func firsthalf(m int32)string{
    hora := ""
    switch m {
        case 1:
            hora = "one minute past "
        case 2:
            hora = "two minutes past "
        case 3:
            hora = "three minutes past "
        case 4:
            hora = "four minutes past "
        case 5:
            hora = "five minutes past "
        case 6:
            hora = "six minutes past "
        case 7:
            hora = "seven minutes past "
        case 8:
            hora = "eight minutes past "
        case 9:
            hora = "nine minutes past "
        case 10:
            hora = "ten minutes past "
        case 11:
            hora = "eleven minutes past "
        case 12:
            hora = "twelve minutes past "
        case 13:
            hora = "thirteen minutes past "
        case 14:
            hora = "fourteen minutes past "
        case 15:
            hora = "quarter past "
        case 16:
            hora = "sixteen minutes past "
        case 17:
            hora = "seventeen minutes past "
        case 18:
            hora = "eighteen minutes past "
        case 19:
            hora = "nineteen minutes past "
        case 20:
            hora = "twenty minutes past "
        case 21:
            hora = "twenty one minutes past "
        case 22:
            hora = "twenty two minutes past "
        case 23:
            hora = "twenty three minutes past "
        case 24:
            hora = "twenty four minutes past " 
        case 25:
            hora = "twenty five minutes past "
        case 26:
            hora = "twenty six minutes past "
        case 27:
            hora = "twenty seven minutes past "
        case 28:
            hora = "twenty eight minutes past "
        case 29:
            hora = "twenty nine minutes past "
        case 30:
            hora = "half past "    
    }
    return hora
}

func otherhalf(m int32)string{
    hora := ""
    switch m {
        case 1:
            hora = "one minute to "
        case 2:
            hora = "two minutes to "
        case 3:
            hora = "three minutes to "
        case 4:
            hora = "four minutes to "
        case 5:
            hora = "five minutes to "
        case 6:
            hora = "six minutes to "
        case 7:
            hora = "seven minutes to "
        case 8:
            hora = "eight minutes to "
        case 9:
            hora = "nine minutes to "
        case 10:
            hora = "ten minutes to "
        case 11:
            hora = "eleven minutes to "
        case 12:
            hora = "twelve minutes to "
        case 13:
            hora = "thirteen minutes to "
        case 14:
            hora = "fourteen minutes to "
        case 15:
            hora = "quarter to "
        case 16:
            hora = "sixteen minutes to "
        case 17:
            hora = "seventeen minutes to "
        case 18:
            hora = "eighteen minutes to "
        case 19:
            hora = "nineteen minutes to "
        case 20:
            hora = "twenty minutes to "
        case 21:
            hora = "twenty one minutes to "
        case 22:
            hora = "twenty two minutes to "
        case 23:
            hora = "twenty three minutes to "
        case 24:
            hora = "twenty four minutes to " 
        case 25:
            hora = "twenty five minutes to "
        case 26:
            hora = "twenty six minutes to "
        case 27:
            hora = "twenty seven minutes to "
        case 28:
            hora = "twenty eight minutes to "
        case 29:
            hora = "twenty nine minutes to "    
    }
    return hora
}

func Compare(n string, s string) bool{
    return n == s
}