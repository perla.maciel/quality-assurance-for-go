package src

import 	( 
    "math"
    "testing"
    "testing/quick"
)


func TestQuickEx2(t *testing.T){
    f := func(x float64) bool {
		y := Ex2(x)
		if y == 0 {
		    return true
		}
		return y == math.Pow(x,2)
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}

func TestQuickAbsolute(t *testing.T){
    f := func(x float64) bool {
		y := Absolute(x)
		return y == math.Abs(x)
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}